import "dotenv/config";
import express from "express";
import path from "path";

const app = express();

app.use(express.static(path.resolve("./dist")));
app.use((req, res) => {
  res.sendFile(path.resolve("./dist/index.html"));
});

app.listen(80, () => {
  console.log("Server listening...\n\tPort: 80");
});
