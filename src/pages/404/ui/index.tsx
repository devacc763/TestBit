import React from "react";
import {Navigate} from "react-router-dom";

const NotFoundPage: React.FC = () => <Navigate to="/" />;

export {NotFoundPage};
