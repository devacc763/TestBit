import {Main} from "@/shared/ui/main";
import {OrganizationBlock} from "@/widgets/organization";
import {Users} from "@/widgets/user";
import React from "react";

const OrganizationPage: React.FC = () => {
  return (
    <Main>
      <OrganizationBlock>
        <Users />
      </OrganizationBlock>
    </Main>
  );
};

export {OrganizationPage};
