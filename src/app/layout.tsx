import {Header} from "@/widgets/header";
import React from "react";
import {Outlet} from "react-router-dom";

const AppLayout: React.FC = () => (
  <>
    <Header />
    <Outlet />
  </>
);

export {AppLayout};
