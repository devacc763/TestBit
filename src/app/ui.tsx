import {NotFoundPage} from "@/pages/404";
import {OrganizationPage} from "@/pages/organization";
import {GlobalStyle} from "@/shared/styles";
import React from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {AppLayout} from "./layout";

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<AppLayout />}>
          <Route index element={<OrganizationPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Route>
      </Routes>
      <GlobalStyle />
    </BrowserRouter>
  );
};

export {App};
