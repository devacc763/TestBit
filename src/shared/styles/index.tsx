import "normalize.css";
import "./fonts.css";
import {createGlobalStyle} from "styled-components";

const GlobalStyle = createGlobalStyle`
  body {
    display: flex;
    min-height: 100vh;
    background: #0E0C15;
    min-width: 350px;
  }

  #root {
    display: flex;
    flex-direction: column;
    width: 100%;
  }

  * {
    box-sizing: border-box;
  }

  *::selection {
    background: #222b44;
  }
`;

export {GlobalStyle};
