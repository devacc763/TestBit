import React from "react";
import ReactPaginate, {ReactPaginateProps} from "react-paginate";
import {styled} from "styled-components";
import arrowNarrowLeftIcon from "@/shared/assets/arrow-narrow-left.svg";
import arrowNarrowRightIcon from "@/shared/assets/arrow-narrow-right.svg";

interface IPaginate extends ReactPaginateProps {
  className?: string;
}

const Paginate: React.FC<IPaginate> = ({className, ...props}) => {
  if (props.pageCount <= 1) {
    return null;
  }

  return (
    <StyledPaginate className={className}>
      <ReactPaginate
        {...props}
        containerClassName="container"
        pageClassName="page"
        pageLinkClassName="link"
        activeClassName="active"
        previousClassName="action prev"
        previousLinkClassName="link"
        nextClassName="action next"
        nextLinkClassName="link"
        disabledClassName="disabled"
        breakClassName="break"
        breakLinkClassName="link"
        breakLabel="...."
        previousLabel={<img src={arrowNarrowLeftIcon} width="12" height="12" />}
        nextLabel={<img src={arrowNarrowRightIcon} width="12" height="12" />}
      />
    </StyledPaginate>
  );
};

const StyledPaginate = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
  > .container {
    display: flex;
    align-items: center;
    gap: 4px;
    list-style: none;
    margin: 0px;
    padding: 0px;
    user-select: none;
    > .action {
      display: inline-flex;
      > .link {
        display: inline-flex;
        padding: 6px 10px;
        cursor: pointer;
      }
      &.disabled {
        > .link {
          cursor: not-allowed;
        }
      }
    }
    > .page {
      display: inline-flex;
      > .link {
        display: inline-flex;
        padding: 8px 14px;
        color: white;
        font-family: "ibmplexsans-medium";
        font-size: 16px;
        font-style: normal;
        line-height: 22px;
        cursor: pointer;
      }
      &.active {
        > .link {
          background: #1c64f2;
          border-radius: 8px;
        }
      }
    }
    > .break {
      display: inline-flex;
      > .link {
        display: inline-flex;
        padding: 10px 14px;
        color: white;
        font-family: "ibmplexsans-medium";
        font-size: 16px;
        font-style: normal;
        line-height: 22px;
        cursor: pointer;
      }
    }
  }
`;

export {Paginate};
export type {IPaginate};
