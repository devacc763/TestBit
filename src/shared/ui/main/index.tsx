import {styled} from "styled-components";

const Main = styled.main`
  margin: 34px 25px;
  margin-top: 0px;
  @media (max-width: 625px) {
    margin: 0px;
  }
`;

export {Main};
