import React from "react";
import {css, styled} from "styled-components";
import {Text} from "@/shared/ui/text";

interface IBlock {
  title?: string;
  className?: string;
  children?: React.ReactNode;
}

const Block: React.FC<IBlock> = ({title, className, children}) => {
  const ex = !!title;

  return (
    <StyledBlock className={className} $ex={ex}>
      {ex && (
        <BlockTop>
          <BlockTitle>{title}</BlockTitle>
        </BlockTop>
      )}
      {ex ? <BlockBottom>{children}</BlockBottom> : children}
    </StyledBlock>
  );
};

interface IStyledBlock {
  $ex: boolean;
}

const StyledBlock = styled.div<IStyledBlock>`
  background: #121825;
  border-radius: 18px;
  ${({$ex}) =>
    $ex &&
    css`
      @media (max-width: 625px) {
        border-radius: 0px;
      }
    `}
  ${({$ex}) =>
    !$ex &&
    css`
      padding: 16px 24px;
      @media (max-width: 1000px) {
        padding: 14px 18px;
      }
      @media (max-width: 625px) {
        padding: 10px 16px;
      }
    `}
`;

const BlockTop = styled.div`
  padding: 24px 34px;
  border-bottom: 1px solid #222b44;
  @media (max-width: 1000px) {
    padding: 24px;
  }
  @media (max-width: 625px) {
    padding: 18px 16px;
  }
`;

const BlockTitle = styled(Text).attrs({type: "xl-semibold"})``;

const BlockBottom = styled.div`
  padding: 29px 34px;
  @media (max-width: 1000px) {
    padding: 24px;
  }
  @media (max-width: 625px) {
    padding: 18px 16px;
  }
`;

export {Block};
