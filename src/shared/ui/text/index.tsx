import React from "react";
import {styled, css} from "styled-components";
import {NavLink} from "react-router-dom";

type TextType = "xl-semibold" | "m-medium" | "xs-regular" | "s-medium" | "input-sm";

type TextAs = "h1" | "h2" | "h3" | "span" | typeof NavLink;

interface IText {
  className?: string;
  type?: TextType;
  as?: TextAs;
  to?: string;
  children?: React.ReactNode;
}

const Text: React.FC<IText> = ({type = "s-medium", as = "span", to, className, children}) => (
  <StyledText className={className} as={as} to={to} $type={type}>
    {children}
  </StyledText>
);

interface IStyledText {
  $type: TextType;
}

const StyledText = styled.span<IStyledText>`
  color: white;
  text-decoration: none;
  font-weight: normal;
  ${({$type}) => {
    switch ($type) {
      case "xl-semibold":
        return css`
          font-family: "ibmplexsans-semi-bold";
          font-size: 22px;
          font-style: normal;
          line-height: 29px;
        `;
      case "m-medium":
        return css`
          font-family: "ibmplexsans-medium";
          font-size: 16px;
          font-style: normal;
          line-height: 22px;
        `;
      case "xs-regular":
        return css`
          font-family: "ibmplexsans-regular";
          font-size: 12px;
          font-style: normal;
          line-height: 16px;
        `;
      case "s-medium":
        return css`
          font-family: "ibmplexsans-medium";
          font-size: 14px;
          font-style: normal;
          line-height: 18px;
        `;
      case "input-sm":
        return css`
          font-family: "ibmplexsans-regular";
          font-size: 14px;
          font-style: normal;
          line-height: 18px;
        `;
    }
  }}
`;

export {Text};
