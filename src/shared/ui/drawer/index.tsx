import {AnimatePresence, motion} from "framer-motion";
import React from "react";
import {styled} from "styled-components";
import {createPortal} from "react-dom";
import {Text} from "@/shared/ui/text";
import closeIcon from "@/shared/assets/close.svg";

interface IDrawer {
  opened: boolean;
  title: string;
  onClose?: () => void;
  children?: React.ReactNode;
}

const Drawer: React.FC<IDrawer> = ({opened, title, onClose, children}) => {
  return createPortal(
    <AnimatePresence>
      {opened && (
        <StyledDrawer>
          <DrawerBackground onClick={onClose} />
          <DrawerBlock
            variants={{
              opened: {
                right: 0
              },
              closed: {
                right: -470
              }
            }}
            initial="closed"
            animate="opened"
            exit="closed"
            transition={{
              duration: 0.18
            }}
          >
            <DrawerBlockContent>
              <DrawerBlockTop>
                <DrawerTitle>{title}</DrawerTitle>
                <DrawerCloseButton onClick={onClose}>
                  <img src={closeIcon} width="24" height="24" />
                </DrawerCloseButton>
              </DrawerBlockTop>
              <DrawerBlockBottom>
                <DrawerBlockBottomContent>{children}</DrawerBlockBottomContent>
              </DrawerBlockBottom>
            </DrawerBlockContent>
          </DrawerBlock>
        </StyledDrawer>
      )}
    </AnimatePresence>,
    document.getElementById("portal") ?? document.body
  );
};

const StyledDrawer = styled.div`
  position: absolute;
`;

const DrawerBackground = styled.div`
  position: fixed;
  top: 0px;
  bottom: 0px;
  left: 0px;
  right: 0px;
  opacity: 0.6;
  background: #000;
`;

const DrawerBlock = styled(motion.div)`
  display: flex;
  flex-direction: column;
  position: fixed;
  top: 0px;
  bottom: 0px;
  right: 0px;
  width: 100%;
  height: 100%;
  max-width: 470px;
  min-width: 350px;
  background: #121825;
  overflow: hidden;
`;

const DrawerTitle = styled(Text).attrs({type: "xl-semibold"})``;

const DrawerBlockContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

const DrawerBlockTop = styled.div`
  display: flex;
  width: 100%;
  padding: 56px 20px;
  padding-right: 25px;
  padding-bottom: 20px;
  align-items: center;
  justify-content: space-between;
  @media (max-width: 1000px) {
    padding-bottom: 18px;
  }
  @media (max-width: 625px) {
    padding: 30px 16px;
    padding-bottom: 14px;
  }
`;

const DrawerBlockBottom = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

const DrawerBlockBottomContent = styled.div`
  width: 100%;
  height: 100%;
  padding: 20px;
  padding-top: 0px;
  overflow: auto;
  @media (max-width: 625px) {
    padding: 14px 16px;
    padding-top: 0px;
  }
`;

const DrawerCloseButton = styled.button`
  display: inline-flex;
  padding: 0px;
  border: none;
  outline: none;
  background: none;
  cursor: pointer;
`;

export {Drawer};
