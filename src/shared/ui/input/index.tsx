import React from "react";
import {styled} from "styled-components";
import {InputSearchIcon} from "./search-icon";

interface IInput extends React.ComponentProps<"input"> {}

const Input: React.FC<IInput> = ({className, type = "text", ...props}) => {
  return (
    <StyledInput className={className}>
      {type === "search" && <InputSearchIcon />}
      <InputNative type={type} {...props} />
    </StyledInput>
  );
};

const StyledInput = styled.label`
  display: flex;
  column-gap: 10px;
  align-items: center;
  width: 100%;
  max-width: 600px;
  padding: 0px 16px;
  height: 46px;
  cursor: text;
  border: 1px solid #313e62;
  border-radius: 8px;
  background: #121825;
  &:hover {
    border-color: #1c64f2;
  }
  &:focus-within {
    border-color: #1c64f2;
    background-color: #313e62;
    svg path {
      fill: white;
    }
  }
`;

const InputNative = styled.input`
  display: inline-flex;
  width: 100%;
  height: 100%;
  background: none;
  padding: 0px;
  outline: none;
  border: none;
  color: white;
  font-family: "ibmplexsans-regular";
  font-size: 14px;
  font-style: normal;
  line-height: 18px;
  &::placeholder {
    color: #616d8d;
  }
  &::-webkit-search-decoration,
  &::-webkit-search-cancel-button,
  &::-webkit-search-results-button,
  &::-webkit-search-results-decoration {
    -webkit-appearance: none;
  }
`;

export {Input};
