import {styled, css} from "styled-components";
import {StyledTableCell, TableCell, TableCellSkeleton} from "./cell";
import React from "react";

interface ITable extends React.ComponentProps<"table"> {}

const Table: React.FC<ITable> = ({children, ...props}) => {
  return (
    <StyledTable {...props}>
      <TableNative>{children}</TableNative>
    </StyledTable>
  );
};

const StyledTable = styled.div`
  overflow: auto;
`;

const TableNative = styled.table`
  border-collapse: separate;
  border-spacing: 0px;
  width: inherit;
`;

interface ITableRow {
  clickable?: boolean;
  onClick?: React.MouseEventHandler;
  children?: React.ReactNode;
}

const TableRow: React.FC<ITableRow> = ({clickable = false, onClick, children}) => (
  <StyledTableRow $clickable={clickable} onClick={onClick}>
    {children}
  </StyledTableRow>
);

interface IStyledTableRow {
  $clickable: boolean;
}

const StyledTableRow = styled.tr<IStyledTableRow>`
  ${({$clickable}) =>
    $clickable &&
    css`
      &:hover {
        ${StyledTableCell} {
          background: #1b2336;
        }
      }
      ${StyledTableCell} {
        cursor: pointer;
      }
    `}
`;

const TableHead = styled.thead`
  ${StyledTableCell} {
    cursor: default;
    background: #0e0c15;
    color: #9ca3af;
    &:first-child {
      border-top-left-radius: 8px;
      border-bottom-left-radius: 8px;
    }
    &:last-child {
      border-top-right-radius: 8px;
      border-bottom-right-radius: 8px;
    }
  }
`;

const TableBody = styled.tbody`
  ${StyledTableCell} {
    padding: 23px 14px;
    border-bottom: 1px solid #222b44;
  }
`;

const TableActions = styled.div`
  display: inline-flex;
  align-items: center;
  column-gap: 10px;
`;

const TableAction = styled.button`
  display: inline-flex;
  cursor: pointer;
  padding: 0px;
  border: none;
  background: none;
  outline: none;
`;

export {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableCellSkeleton,
  TableActions,
  TableAction
};
