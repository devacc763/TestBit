import React from "react";
import {styled, css, keyframes} from "styled-components";
import arrowNarrowUpIcon from "@/shared/assets/arrow-narrow-up.svg";
import arrowNarrowDownIcon from "@/shared/assets/arrow-narrow-down.svg";
import {Text} from "@/shared/ui/text";

type TableOrderBy = "asc" | "desc";

interface ITableCell {
  head?: boolean;
  orderBy?: TableOrderBy | null;
  onClick?: () => void;
  children?: React.ReactNode;
}

const TableCell: React.FC<ITableCell> = ({
  head = false,
  orderBy = null,
  onClick,
  children,
  ...props
}) => {
  return (
    <StyledTableCell as={head ? "th" : "td"} $orderBy={!!orderBy} {...props} onClick={onClick}>
      <TableCellText>
        {orderBy == "desc" && <img src={arrowNarrowDownIcon} width="18" height="18" />}
        {orderBy == "asc" && <img src={arrowNarrowUpIcon} width="18" height="18" />}
        {children}
      </TableCellText>
    </StyledTableCell>
  );
};

interface IStyledTableCell {
  $orderBy: boolean;
}

const StyledTableCell = styled.td<IStyledTableCell>`
  padding: 14px;
  text-align: center;
  color: white;
  vertical-align: middle;
  ${({$orderBy}) =>
    $orderBy &&
    css`
      cursor: pointer !important;
      user-select: none;
    `}
`;

const TableCellText = styled(Text)`
  display: inline-flex;
  align-items: center;
  gap: 10px;
  color: inherit;
  vertical-align: middle;
`;

const TableCellSkeletonAnimation = keyframes`
  50% {
    background: #374151;
  }
`;

interface ITableCellSkeleton {
  width?: string;
}

const TableCellSkeleton: React.FC<ITableCellSkeleton> = ({width}) => (
  <StyledTableCellSkeleton $width={width} />
);

interface IStyledTableCellSkeleton {
  $width?: string;
}

const StyledTableCellSkeleton = styled.span<IStyledTableCellSkeleton>`
  display: inline-flex;
  width: ${({$width}) => $width ?? 180}px;
  height: 18px;
  background: #616d8d;
  cursor: progress;
  border-radius: 9px;
  animation: 2s ${TableCellSkeletonAnimation} Infinite ease-in;
  animation-fill-mode: both;
`;

export {TableCell, TableCellSkeleton, StyledTableCell};
