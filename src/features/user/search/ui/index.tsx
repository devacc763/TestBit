import {userModel} from "@/entities/user";
import {Input} from "@/shared/ui/input";
import {useUnit} from "effector-react";
import React, {useCallback} from "react";
import {styled} from "styled-components";

interface IUserSearchInput {
  className?: string;
}

const UserSearchInput: React.FC<IUserSearchInput> = ({className}) => {
  const {search, setSearch} = useUnit({
    search: userModel.$search,
    setSearch: userModel.setSearch
  });

  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setSearch(event.target.value);
    },
    [setSearch]
  );

  return (
    <StyledUserSearchInput
      className={className}
      type="search"
      value={search}
      onChange={handleChange}
      placeholder="Поиск"
    />
  );
};

const StyledUserSearchInput = styled(Input)``;

export {UserSearchInput};
