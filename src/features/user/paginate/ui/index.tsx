import {userModel} from "@/entities/user";
import {IPaginate, Paginate} from "@/shared/ui/paginate";
import {useUnit} from "effector-react";
import React, {useCallback} from "react";

interface IUserPaginate {
  className?: string;
}

const UserPaginate: React.FC<IUserPaginate> = ({className}) => {
  const {pages, setPage, page} = useUnit({
    pages: userModel.$pages,
    setPage: userModel.setPage,
    page: userModel.$page
  });

  const handlePageChange = useCallback<Exclude<IPaginate["onPageChange"], undefined>>(
    (selectedItem) => {
      setPage(selectedItem.selected + 1);
    },
    [setPage]
  );

  return (
    <Paginate
      className={className}
      initialPage={page - 1}
      pageCount={pages}
      onPageChange={handlePageChange}
      pageRangeDisplayed={4}
      marginPagesDisplayed={1}
    />
  );
};

export {UserPaginate};
