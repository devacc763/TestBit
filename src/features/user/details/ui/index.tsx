import {userModel} from "@/entities/user";
import {Drawer} from "@/shared/ui/drawer";
import {useUnit} from "effector-react";
import React from "react";
import {styled} from "styled-components";

interface IUserDetails {
  children?: React.ReactNode;
}

const UserDetails: React.FC<IUserDetails> = ({children}) => {
  const {opened, close, user} = useUnit({
    opened: userModel.$drawerOpened,
    close: userModel.closeDrawer,
    user: userModel.$user
  });

  if (user === null) {
    return null;
  }
  return (
    <Drawer opened={opened} title={user.email} onClose={close}>
      <UserDetailsContent>{children}</UserDetailsContent>
    </Drawer>
  );
};

const UserDetailsContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  row-gap: 20px;
  @media (max-width: 1000px) {
    row-gap: 18px;
  }
  @media (max-width: 625px) {
    row-gap: 14px;
  }
`;

export {UserDetails};
