import {Block} from "@/shared/ui/block";
import React from "react";

interface IOrganizationBlock {
  children?: React.ReactNode;
}

const OrganizationBlock: React.FC<IOrganizationBlock> = ({children}) => {
  return <Block title="Моя организация">{children}</Block>;
};

export {OrganizationBlock};
