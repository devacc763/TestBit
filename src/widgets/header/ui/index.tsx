import React from "react";
import {styled} from "styled-components";
import {NavLink} from "react-router-dom";
import defaultAvatarImage from "@/shared/assets/default-avatar.png";
import organizationIcon from "@/shared/assets/organization.svg";
import {Block} from "@/shared/ui/block";
import {Text} from "@/shared/ui/text";

const Header: React.FC = () => {
  return (
    <StyledHeader>
      <HeaderLeft>
        <HeaderLogo>BitTest</HeaderLogo>
        <HeaderNav>
          <HeaderNavLink to="/">
            <img src={organizationIcon} width={24} height={24} alt="Моя организация" />
            Моя организация
          </HeaderNavLink>
        </HeaderNav>
      </HeaderLeft>
      <HeaderRight>
        <HeaderUserBlock>
          <img src={defaultAvatarImage} width={32} height={32} alt="Администратор" />
          <HeaderUserTexts>
            <HeaderUserDesc>Вы авторизованы</HeaderUserDesc>
            <HeaderUserName>Администратор</HeaderUserName>
          </HeaderUserTexts>
        </HeaderUserBlock>
      </HeaderRight>
    </StyledHeader>
  );
};

const StyledHeader = styled(Block)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 34px 25px;
  @media (max-width: 625px) {
    margin: 24px 16px;
  }
`;

const HeaderLeft = styled.div`
  display: flex;
  align-items: center;
  @media (max-width: 625px) {
    width: 100%;
    justify-content: space-between;
  }
`;

const HeaderRight = styled.div`
  @media (max-width: 625px) {
    display: none;
  }
`;

const HeaderLogo = styled(Text).attrs({type: "xl-semibold"})`
  cursor: default;
`;

const HeaderNav = styled.nav`
  display: flex;
  align-items: center;
  column-gap: 20px;
`;

const HeaderNavLink = styled(NavLink)`
  display: inline-flex;
  align-items: center;
  column-gap: 10px;
  color: white;
  text-decoration: none;
  margin-left: 70px;
  font-family: "ibmplexsans-medium";
  font-size: 16px;
  font-style: normal;
  line-height: 22px;
  flex-shrink: 0;
  @media (max-width: 625px) {
    margin-left: 20px;
  }
`;

const HeaderUserBlock = styled.div`
  display: flex;
  align-items: center;
  column-gap: 12px;
  border: 1px solid #222b44;
  border-radius: 6px;
  padding: 9px 14px;
  cursor: default;
`;

const HeaderUserTexts = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

const HeaderUserDesc = styled(Text).attrs({type: "xs-regular"})`
  color: #616d8d;
`;

const HeaderUserName = styled(Text).attrs({type: "s-medium"})``;

export {Header};
