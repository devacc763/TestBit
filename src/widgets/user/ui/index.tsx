import {TransactionExpenses, TransactionHistory} from "@/entities/transaction";
import {UserTable} from "@/entities/user";
import {UserDetails} from "@/features/user/details";
import {UserPaginate} from "@/features/user/paginate";
import {UserSearchInput} from "@/features/user/search";
import {Text} from "@/shared/ui/text";
import React from "react";
import {styled} from "styled-components";

const Users: React.FC = () => {
  return (
    <StyledUsers>
      <UsersTitle>Пользователи</UsersTitle>
      <StyledUserSearchInput />
      <StyledUserTable />
      <StyledUserPaginate />
      <UserDetails>
        <TransactionExpenses />
        <TransactionHistory />
      </UserDetails>
    </StyledUsers>
  );
};

const StyledUsers = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

const UsersTitle = styled(Text).attrs({type: "xl-semibold"})``;

const StyledUserSearchInput = styled(UserSearchInput)`
  margin-top: 24px;
`;

const StyledUserTable = styled(UserTable)`
  margin-top: 24px;
`;

const StyledUserPaginate = styled(UserPaginate)`
  margin-top: 24px;
`;

export {Users};
