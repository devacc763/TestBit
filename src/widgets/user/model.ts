import {getUserTransactions} from "@/entities/transaction/model";
import {IUser, userModel} from "@/entities/user";
import {sample} from "effector";

sample({
  source: userModel.$user,
  filter: (user) => user !== null,
  target: [
    getUserTransactions.prepend<IUser | null>((user) => ({
      id: user?.id ?? ""
    }))
  ]
});

export * from "@/entities/user/model";
