import {combine, createEffect, createEvent, createStore, sample} from "effector";
import {GetUserListParams, GetUserListResult, IUser, UserApi, UserOrderBy} from ".";

const openDrawer = createEvent();

const closeDrawer = createEvent();

const $drawerOpened = createStore(false)
  .on(openDrawer, () => true)
  .on(closeDrawer, () => false);

const rowClicked = createEvent<IUser>();

const $user = createStore<IUser | null>(null).on(rowClicked, (_, user) => user);

sample({
  clock: rowClicked,
  target: [openDrawer]
});

const getUsers = createEffect<GetUserListParams, GetUserListResult>({
  handler: async (params) => {
    const {data} = await UserApi.getList(params);

    return data;
  }
});

const $loading = combine([getUsers.pending], ([pending]) => pending);

const setSearch = createEvent<string>();

const $search = createStore("").on(setSearch, (_, search) => search);

const setPage = createEvent<number>();

const $page = createStore(1).on(setPage, (_, page) => page);

const toggleOrderBy = createEvent();

const $orderBy = createStore<UserOrderBy | null>(null).on(toggleOrderBy, (orderBy) =>
  orderBy === UserOrderBy.TOKENS_DESC ? UserOrderBy.TOKENS_ASC : UserOrderBy.TOKENS_DESC
);

const loadUsers = createEvent();

sample({
  clock: loadUsers,
  source: combine({
    search: $search,
    page: $page,
    orderBy: $orderBy
  }),
  target: [getUsers]
});

sample({
  source: combine({
    search: $search,
    page: $page,
    orderBy: $orderBy
  }),
  target: [getUsers]
});

const $users = createStore<IUser[]>([]).on(getUsers.doneData, (_, {data}) => data);

const $pages = createStore(1).on(getUsers.doneData, (_, {pages}) => pages);

export {
  openDrawer,
  closeDrawer,
  $drawerOpened,
  rowClicked,
  getUsers,
  $loading,
  setSearch,
  $search,
  setPage,
  $page,
  toggleOrderBy,
  $orderBy,
  loadUsers,
  $users,
  $pages,
  $user
};
