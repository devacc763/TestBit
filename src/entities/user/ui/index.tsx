import {
  Table,
  TableAction,
  TableActions,
  TableBody,
  TableCell,
  TableCellSkeleton,
  TableHead,
  TableRow
} from "@/shared/ui/table";
import React, {useEffect} from "react";
import {styled} from "styled-components";
import editIcon from "@/shared/assets/edit.svg";
import trashIcon from "@/shared/assets/trash.svg";
import {useUnit} from "effector-react";
import {UserOrderBy, userModel} from "@/entities/user";

interface IUserTable {
  className?: string;
}

const UserTable: React.FC<IUserTable> = ({className}) => {
  const {loadUsers, loading, users, rowClicked, orderBy, toggleOrderBy} = useUnit({
    loadUsers: userModel.loadUsers,
    loading: userModel.$loading,
    rowClicked: userModel.rowClicked,
    users: userModel.$users,
    orderBy: userModel.$orderBy,
    toggleOrderBy: userModel.toggleOrderBy
  });

  useEffect(() => {
    loadUsers();
  }, []);

  return (
    <StyledUserTable className={className}>
      <TableHead>
        <TableRow>
          <TableCell head>Email</TableCell>
          <TableCell head>Имя</TableCell>
          <TableCell head>Роль</TableCell>
          <TableCell head>Подписка</TableCell>
          <TableCell
            head
            orderBy={orderBy !== UserOrderBy.TOKENS_DESC ? "asc" : "desc"}
            onClick={toggleOrderBy}
          >
            Токены
          </TableCell>
          <TableCell head>Действия</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {loading &&
          [...Array(20)].map((_, index) => (
            <TableRow key={index}>
              <TableCell>
                <TableCellSkeleton width="132" />
              </TableCell>
              <TableCell>
                <TableCellSkeleton width="50" />
              </TableCell>
              <TableCell>
                <TableCellSkeleton width="50" />
              </TableCell>
              <TableCell>
                <TableCellSkeleton width="50" />
              </TableCell>
              <TableCell>
                <TableCellSkeleton />
              </TableCell>
              <TableCell>
                <TableCellSkeleton width="50" />
              </TableCell>
            </TableRow>
          ))}
        {!loading &&
          users.map((user) => (
            <TableRow key={user.id} clickable onClick={rowClicked.bind(null, user)}>
              <TableCell>{user.email}</TableCell>
              <TableCell>{user.name}</TableCell>
              <TableCell>{user.role}</TableCell>
              <TableCell>{user.subscription.plan.type}</TableCell>
              <TableCell>{user.subscription.tokens.toLocaleString("ru")} TKN</TableCell>
              <TableCell>
                <TableActions>
                  <TableAction>
                    <img src={editIcon} width="18" height="18" />
                  </TableAction>
                  <TableAction>
                    <img src={trashIcon} width="18" height="18" />
                  </TableAction>
                </TableActions>
              </TableCell>
            </TableRow>
          ))}
      </TableBody>
    </StyledUserTable>
  );
};

const StyledUserTable = styled(Table)`
  width: 100%;
  max-width: 1800px;
`;

export {UserTable};
