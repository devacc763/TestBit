import {api} from "@/shared/api";
import {GetUserListParams, GetUserListResult} from "./types";
import {AxiosPromise} from "axios";

namespace UserApi {
  export function getList(params: GetUserListParams): AxiosPromise<GetUserListResult> {
    return api.get<GetUserListResult>("/user/list", {params});
  }
}

export {UserApi};
export * from "./types";
