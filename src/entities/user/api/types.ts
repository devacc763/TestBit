enum UserRole {
  USER = "USER"
}

enum UserOrderBy {
  TOKENS_ASC = "tokens:asc",
  TOKENS_DESC = "tokens:desc"
}

enum PlanType {
  FREE = "FREE",
  BASIC = "BASIC",
  PREMIUM = "PREMIUM",
  ELITE = "ELITE"
}

interface IPlan {
  id: string;
  type: PlanType;
  price: number;
  currency: string;
  tokens: number;
}

interface ISubscription {
  id: string;
  plan_id: string;
  user_id: string;
  tokens: number;
  additional_tokens: number;
  created_at: string;
  plan: IPlan;
}

interface IUser {
  id: string;
  email: string;
  tg_id: null;
  name: string;
  password: null;
  avatar: null;
  created_at: string;
  role: UserRole;
  subscription: ISubscription;
}

interface GetUserListParams {
  page?: number;
  search?: string;
  orderBy?: UserOrderBy | null;
}

interface GetUserListResult {
  data: IUser[];
  pages: number;
}

export {UserRole, UserOrderBy};
export type {IUser, GetUserListParams, GetUserListResult};
