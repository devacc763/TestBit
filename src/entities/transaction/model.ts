import {combine, createEffect, createStore} from "effector";
import * as echarts from "echarts";
import {
  GetUserTransactionListParams,
  GetUserTransactionListResult,
  ITransaction,
  TransactionApi,
  TransactionCurrency,
  TransactionType
} from ".";

const getUserTransactions = createEffect<
  GetUserTransactionListParams,
  GetUserTransactionListResult
>({
  handler: async (params) => {
    const {data} = await TransactionApi.getUserList(params);

    return data;
  }
});

const $transactions = createStore<ITransaction[]>([]).on(
  getUserTransactions.doneData,
  (_, transactions) => transactions
);

const $loading = combine([getUserTransactions.pending], ([pending]) => pending);

const $expenses = combine([$transactions], ([transactions]) =>
  [...transactions]
    .reverse()
    .filter(
      (transaction) =>
        transaction.type === TransactionType.WRITE_OFF &&
        transaction.currency === TransactionCurrency.SYSTEM_TOKEN
    )
);

const $expensesChart = combine([$expenses], ([expenses]) => ({
  grid: {
    left: "3%",
    right: "10%",
    top: "15%",
    bottom: "20%",
    containLabel: true,
    borderColor: "red"
  },
  dataZoom: [
    {
      type: "inside"
    },
    {
      type: "slider",
      show: true,
      bottom: 0
    }
  ],
  xAxis: {
    data: expenses.map((expense) =>
      new Date(expense.created_at).toLocaleString("ru", {hour: "2-digit", minute: "2-digit"})
    ),
    boundaryGap: false,
    axisLabel: {
      color: "#616D8D"
    },
    axisLine: {
      lineStyle: {
        color: "#222B44"
      }
    }
  },
  yAxis: {
    type: "value",
    axisLabel: {
      color: "#616D8D",
      // eslint-disable-next-line
      formatter: (value: any) => value.toLocaleString("ru")
    },
    splitLine: {
      lineStyle: {
        color: "#222B44"
      }
    }
  },
  series: [
    {
      data: expenses.map((expense) => expense.amount),
      type: "line",
      showSymbol: false,
      smooth: true,
      lineStyle: {
        color: "#1C64F2",
        width: 2
      },
      areaStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: "rgba(28, 100, 242, 1)"
          },
          {
            offset: 1,
            color: "rgba(28, 100, 242, 0)"
          }
        ]),
        opacity: 0.15
      }
    }
  ]
}));

export {getUserTransactions, $transactions, $loading, $expenses, $expensesChart};
