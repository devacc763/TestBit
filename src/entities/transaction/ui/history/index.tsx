import {
  Table,
  TableBody,
  TableCell,
  TableCellSkeleton,
  TableHead,
  TableRow
} from "@/shared/ui/table";
import {Text} from "@/shared/ui/text";
import {useUnit} from "effector-react";
import React from "react";
import {styled} from "styled-components";
import {TransactionCurrency, TransactionType, transactionModel} from "@/entities/transaction";

const TransactionHistory: React.FC = () => {
  const {transactions, loading} = useUnit({
    transactions: transactionModel.$transactions,
    loading: transactionModel.$loading
  });

  return (
    <StyledTransactionHistory>
      <TransactionHistoryTitle>История операций</TransactionHistoryTitle>
      <TransactionHistoryTable>
        <TableHead>
          <TableRow>
            <TableCell head>Тип</TableCell>
            <TableCell head>Сумма</TableCell>
            <TableCell head>Дата</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {loading &&
            [...Array(20)].map((_, index) => (
              <TableRow key={index}>
                <TableCell>
                  <TableCellSkeleton width="65" />
                </TableCell>
                <TableCell>
                  <TableCellSkeleton width="90" />
                </TableCell>
                <TableCell>
                  <TableCellSkeleton width="125" />
                </TableCell>
              </TableRow>
            ))}
          {!loading &&
            transactions.map((transaction) => {
              let typeText: string;
              switch (transaction.type) {
                case TransactionType.WRITE_OFF:
                  typeText = "Списание";
                  break;
                case TransactionType.REPLENISH:
                  typeText = "Пополнение";
                  break;
                default:
                  typeText = transaction.type;
                  break;
              }

              let amount = "";
              let amountColor: "green" | "red";
              switch (transaction.type) {
                case TransactionType.WRITE_OFF:
                  amount = "-";
                  amountColor = "red";
                  break;
                case TransactionType.REPLENISH:
                  amount = "+";
                  amountColor = "green";
                  break;
              }
              amount += transaction.amount.toLocaleString("ru") + " ";
              switch (transaction.currency) {
                case TransactionCurrency.SYSTEM_TOKEN:
                  amount += "BTKN";
                  break;
                default:
                  amount += transaction.currency;
                  break;
              }

              const dateCreatedText = new Date(transaction.created_at).toLocaleString("ru", {
                year: "2-digit",
                month: "2-digit",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
                second: "2-digit"
              });

              return (
                <TableRow key={transaction.id}>
                  <TableCell>{typeText}</TableCell>
                  <TransactionHistoryTableAmountCell $color={amountColor}>
                    {amount}
                  </TransactionHistoryTableAmountCell>
                  <TableCell>{dateCreatedText}</TableCell>
                </TableRow>
              );
            })}
        </TableBody>
      </TransactionHistoryTable>
    </StyledTransactionHistory>
  );
};

const StyledTransactionHistory = styled.div``;

const TransactionHistoryTitle = styled(Text).attrs({type: "xl-semibold"})``;

const TransactionHistoryTable = styled(Table)`
  width: 100%;
  margin-top: 20px;
`;

interface ITransactionHistoryTableAmountCell {
  $color: "green" | "red";
}

const TransactionHistoryTableAmountCell = styled(TableCell)<ITransactionHistoryTableAmountCell>`
  color: ${({$color}) => {
    switch ($color) {
      case "green":
        return "#1ABB34";
      case "red":
        return "#FE4242";
    }
  }};
`;

export {TransactionHistory};
