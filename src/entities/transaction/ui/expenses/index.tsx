import {Text} from "@/shared/ui/text";
import React from "react";
import {styled} from "styled-components";
import ReactECharts from "echarts-for-react";
import {useUnit} from "effector-react";
import {transactionModel} from "@/entities/transaction";

const TransactionExpenses: React.FC = () => {
  const {expensesChart} = useUnit({
    expensesChart: transactionModel.$expensesChart
  });

  return (
    <StyledTransactionExpenses>
      <TransactionExpensesTitle>Использование токенов</TransactionExpensesTitle>
      <TransactionExpensesChart>
        <TransactionExpensesChartNative option={expensesChart} />
      </TransactionExpensesChart>
    </StyledTransactionExpenses>
  );
};

const StyledTransactionExpenses = styled.div`
  border-bottom: 1px solid #222b44;
  padding-bottom: 20px;
  @media (max-width: 1000px) {
    padding-bottom: 18px;
  }
  @media (max-width: 625px) {
    padding-bottom: 14px;
  }
`;

const TransactionExpensesTitle = styled(Text).attrs({type: "xl-semibold"})``;

const TransactionExpensesChart = styled.div``;

const TransactionExpensesChartNative = styled(ReactECharts)``;

export {TransactionExpenses};
