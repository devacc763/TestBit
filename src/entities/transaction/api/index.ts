import {AxiosPromise} from "axios";
import {GetUserTransactionListParams, GetUserTransactionListResult} from "./types";
import {api} from "@/shared/api";

namespace TransactionApi {
  export function getUserList({
    id,
    ...params
  }: GetUserTransactionListParams): AxiosPromise<GetUserTransactionListResult> {
    return api.get<GetUserTransactionListResult>("/user/" + id + "/transactions", {params});
  }
}

export {TransactionApi};
export * from "./types";
