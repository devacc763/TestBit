enum TransactionType {
  WRITE_OFF = "WRITE_OFF",
  REPLENISH = "REPLENISH"
}

enum TransactionCurrency {
  SYSTEM_TOKEN = "SYSTEM_TOKEN",
  RUB = "RUB"
}

interface ITransaction {
  id: string;
  provider: string;
  amount: number;
  currency: TransactionCurrency;
  meta: null;
  status: string;
  type: TransactionType;
  plan_id: null;
  user_id: string;
  referral_id: null;
  created_at: string;
  external_id: null;
}

interface GetUserTransactionListParams {
  id: string;
}

type GetUserTransactionListResult = ITransaction[];

export {TransactionType, TransactionCurrency};
export type {ITransaction, GetUserTransactionListParams, GetUserTransactionListResult};
