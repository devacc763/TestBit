FROM node:20.10.0
WORKDIR /app
COPY . .
RUN yarn
RUN yarn build
CMD [ "yarn", "start" ]
